let canvas = document.getElementById("drawing-board");
let ctx = canvas.getContext("2d");
let eraser = document.getElementById("eraser");
let brush = document.getElementById("brush");
let text = document.getElementById("text");
let reSetCanvas = document.getElementById("clear");
let aColorBtn = document.getElementsByClassName("color-item");
let save = document.getElementById("save");
let undo = document.getElementById("undo");
let range = document.getElementById("range");
let clear = false;
let activeColor = 'black';
let lWidth = 10;
let font = document.getElementById("font");
let fontFamily = "Aerial";

let input = document.getElementById("textinput");

let drawing_a_line = document.getElementById("drawing_a_line");
let triangle = document.getElementById("triangle");
let rect = document.getElementById("rect");
let circle = document.getElementById("circle");
let redo = document.getElementById("redo");
let fill = document.getElementById("fill");
let filling = false;
let rand = document.getElementById("rand");
let randing = false;
let bColorBtn = document.getElementById("favColor");
var theColor = bColorBtn.value;
bColorBtn.addEventListener("input", function() {
  aColorBtn[aColorBtn.length - 1].style.backgroundColor = theColor;
}, false);

let undoData = [];
let redoData = [];
let mode = "pen";

autoSetSize(canvas);

setCanvasBg('transparent');

listenToUser(canvas);

getColor();

window.onbeforeunload = function(){
    return "Reload site?";
};

function autoSetSize(canvas) {
    canvasSetSize();

    function canvasSetSize() {
        let pageWidth = document.documentElement.clientWidth;
        let pageHeight = document.documentElement.clientHeight;

        canvas.width = pageWidth;
        canvas.height = pageHeight;
    }

    window.onresize = function () {
        canvasSetSize();
    }
}

function setCanvasBg(color) {
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    //ctx.fillStyle = "black";
}

function listenToUser(canvas) {
    let painting = false;
    let drawing_line = false;
    let draw_triangle = false;
    let draw_rectangle = false;
    let draw_circle = false;
    let newPoint = {x: undefined, y: undefined};
    let lastPoint = {x: undefined, y: undefined};

    var firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
    saveData(firstDot);
  
    canvas.onmousedown = function (e) {
      
      if(mode == "text"){
        input.style.top = e.clientY + "px";
        input.style.left = e.clientX + "px";
        input.value = "";
        input.style.display = "initial";
        input.focus();
      }
      else if(mode == "pen"){
        painting = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
        drawCircle(x, y, 0);
      }
      else if(mode == "eraser"){
        painting = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
        drawCircle(x, y, 0);
      }
      else if(mode == "drawline"){
        drawing_line = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
      }
      else if(mode == "triangle"){
        draw_triangle = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
      }else if(mode == "rectangle"){
        draw_rectangle = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
      }else if(mode == "circle"){
        draw_circle = true;
        let x = e.clientX;
        let y = e.clientY;
        lastPoint = {"x": x, "y": y};
        ctx.save();
      }
    };
  canvas.onmousemove = function (e) {
    if (painting) {
      let x = e.clientX;
      let y = e.clientY;
      let newPoint = {"x": x, "y": y};
      drawLine(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
      lastPoint = newPoint;
    }
    else if(drawing_line){
      let x = e.clientX;
      let y = e.clientY;
      newPoint = {"x": x, "y": y};
      ctx.putImageData(undoData[undoData.length - 1], 0, 0);
      drawLine2(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
      //lastPoint = newPoint;
    }
    else if(draw_triangle){
      let x = e.clientX;
      let y = e.clientY;
      newPoint = {"x": x, "y": y};
      ctx.putImageData(undoData[undoData.length - 1], 0, 0);
      drawTriangle(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
    }
    else if(draw_circle){
      let x = e.clientX;
      let y = e.clientY;
      newPoint = {"x": x, "y": y};
      ctx.putImageData(undoData[undoData.length - 1], 0, 0);
      drawCircle2(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
    }
    else if(draw_rectangle){
      let x = e.clientX;
      let y = e.clientY;
      newPoint = {"x": x, "y": y};
      ctx.putImageData(undoData[undoData.length - 1], 0, 0);
      drawRectangle(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
    }
    
    canvas.onmouseup = function () {
      painting = false;
      if(drawing_line){drawing_line = false;drawLine2(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);}
      else if(draw_triangle){draw_triangle = false;}
      else if(draw_rectangle){draw_rectangle = false;drawRectangle(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y)}
      else if(draw_circle){draw_circle = false;drawCircle2(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y)}
      newPoint = {x: undefined, y: undefined};
      lastPoint = {x: undefined, y: undefined};
      if(mode != "text"){
      this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
      saveData(this.firstDot);
      }
    };

    canvas.mouseleave = function () {
        painting = false;
        drawing_line = false;
        draw_triangle = false;
        draw_rectangle = false;
        draw_circle = false;
        if(mode != "text"){
          this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
          saveData(this.firstDot);
        }
    }

    
  }

  var colorInput = document.getElementById('favColor');
  colorInput.addEventListener('change', ()=>{
    activeColor = colorInput.value;
    ctx.fillStyle = activeColor;
    ctx.strokeStyle = activeColor;
    for (let i = 0; i < aColorBtn.length; i++) {
      aColorBtn[i].classList.remove("active");
      this.classList.add("active");
      activeColor = this.style.backgroundColor;
      ctx.fillStyle = activeColor;
      ctx.strokeStyle = activeColor;
    }
  }, false)

  document.getElementById("file-uploader").addEventListener('change', function() {
    if (document.getElementById("file-uploader").files && document.getElementById("file-uploader").files[0]) {
        var img = new Image();  // $('img')[0]
        img.onload = imageIsLoaded;
        img.src = URL.createObjectURL(document.getElementById("file-uploader").files[0]); // set src to blob url
    }
  });

}

function imageIsLoaded() { 
  ctx.drawImage(this, 20,20);
}

function changeFont(){
    let index = font.selectedIndex;
    fontFamily = font.options[index].value;
}

function drawText(event){
    if(event.key != "Enter")
        return;
    let value = input.value;
    let x = input.style.left.slice(0, -2);
    let y = input.style.top.slice(0, -2);
    
    ctx.fillStyle = activeColor;
    ctx.font = lWidth + "px " + fontFamily;
    ctx.fillText(value, x, y);
    input.style.display = "none";

    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
    saveData(this.firstDot);
    
}

function drawCircle(x, y, radius) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fill();
    if (clear) {
        ctx.clip();
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    }
}

var rr = 182, rg = 128, rb = 218;

function drawLine(x1, y1, x2, y2) {
    ctx.lineWidth = lWidth;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    rr += Math.floor(Math.random()*10) * ((Math.random()>0.5)?-1:1);
    rg += Math.floor(Math.random()*10) * ((Math.random()>0.5)?-1:1);
    rb += Math.floor(Math.random()*10) * ((Math.random()>0.5)?-1:1);
    if (clear) {
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
        ctx.clip();
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    }else{
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        if(randing){
          activeColor = "rgb("+rr+", "+rg+", "+rb+")";
          ctx.fillStyle = activeColor;
          ctx.strokeStyle = activeColor;
          console.log("rr:"+rr,"rg:"+rg,"rb:"+rb)
        }
        ctx.stroke();
        ctx.closePath();
    }
}

function drawLine2(x1, y1, x2, y2) {
  ctx.lineWidth = lWidth;
  //ctx.lineCap = "round";
  //ctx.lineJoin = "round";
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  //ctx.strokeStyle = activeColor;
  ctx.lineTo(x2, y2);
  ctx.stroke();
  ctx.closePath();
}

function drawTriangle(x1, y1, x2, y2) {//邊畫邊顯示
  ctx.lineWidth = lWidth;
  ctx.beginPath();
  ctx.strokeStyle = activeColor;
  ctx.moveTo(x1, y1); 
  ctx.lineTo(x2, y2);
  console.log("x1:"+x1,"y1:"+y1, "x2:"+x2,"y2:"+ y2)
  ctx.lineTo(x1-(x2-x1), y2);
  ctx.closePath();
  ctx.fillStyle = activeColor;
  if(filling) ctx.fill();
  ctx.stroke();
}

function drawRectangle(x1, y1, x2, y2){//邊畫邊顯示
  ctx.lineWidth = lWidth;
  ctx.beginPath();
  ctx.rect(x1, y1, x2-x1, y2-y1);
  ctx.fillStyle = activeColor;
  if(filling) ctx.fill();
  ctx.stroke();
}

function drawCircle2(x1, y1, x2, y2){
  ctx.lineWidth = lWidth;
  ctx.strokeStyle = activeColor;
  ctx.beginPath();
  ctx.arc((x1+x2)/2, (y1+y2)/2, Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2)), 0, 2 * Math.PI);
  ctx.stroke();
}

range.onchange = function(){
    lWidth = this.value;
};

eraser.onclick = function () {
    mode = "eraser";  
    clear = true;
    input.style.display = "none";
    this.classList.add("active");
    brush.classList.remove("active");
    text.classList.remove("active");
    rect.classList.remove("active");
    triangle.classList.remove("active");
    circle.classList.remove("active");
    drawing_a_line.classList.remove("active");
};

fill.onclick = function() {
  filling = !filling;
  if(filling == true){this.classList.add("active");}
  else{this.classList.remove("active");}
}

rand.onclick = function() {
  randing =  !randing;
  if(randing == true){this.classList.add("active");}
  else{this.classList.remove("active");}
}

brush.onclick = function () {
    mode = "pen";
    input.style.display = "none";
    clear = false;
    this.classList.add("active");
    eraser.classList.remove("active");
    text.classList.remove("active");
    rect.classList.remove("active");
    triangle.classList.remove("active");
    circle.classList.remove("active");
    drawing_a_line.classList.remove("active");
};

triangle.onclick = function () {
  mode = "triangle";
  input.style.display = "none";
  this.classList.add("active");
  brush.classList.remove("active");
  text.classList.remove("active");
  eraser.classList.remove("active");
  rect.classList.remove("active");
  circle.classList.remove("active");
  drawing_a_line.classList.remove("active");
}

rect.onclick = function () {
  mode = "rectangle";
  input.style.display = "none";
  this.classList.add("active");
  brush.classList.remove("active");
  text.classList.remove("active");
  eraser.classList.remove("active");
  triangle.classList.remove("active");
  circle.classList.remove("active");
  drawing_a_line.classList.remove("active");
}

circle.onclick = function () {
  mode = "circle";
  input.style.display = "none";
  this.classList.add("active");
  brush.classList.remove("active");
  text.classList.remove("active");
  eraser.classList.remove("active");
  rect.classList.remove("active");
  triangle.classList.remove("active");
  drawing_a_line.classList.remove("active");
}

drawing_a_line.onclick = function () {
  mode = "drawline";
  input.style.display = "none";
  this.classList.add("active");
  brush.classList.remove("active");
  text.classList.remove("active");
  eraser.classList.remove("active");
  rect.classList.remove("active");
  triangle.classList.remove("active");
  circle.classList.remove("active");
}

text.onclick = function () {
  mode = "text";
  clear = false;
  this.classList.add("active");
  brush.classList.remove("active");
  eraser.classList.remove("active");
  rect.classList.remove("active");
  triangle.classList.remove("active");
  circle.classList.remove("active");
}

reSetCanvas.onclick = function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    input.style.display = "none";
    setCanvasBg('white');
};

save.onclick = function () {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    input.style.display = "none";
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "107062237";
    saveA.target = "_blank";
    saveA.click();
};

function getColor(){
  for (let i = 0; i < aColorBtn.length; i++) {
      aColorBtn[i].onclick = function () {
        let j = i;
          for (let i = 0; i < aColorBtn.length; i++) {
              aColorBtn[i].classList.remove("active");
              this.classList.add("active");
              activeColor = this.style.backgroundColor;
              ctx.fillStyle = activeColor;
              ctx.strokeStyle = activeColor;
          }
      }
  }
}


function saveData (data) {
    (undoData.length === 1000) && (undoData.shift());// 上限为储存10步，太多了怕挂掉
    undoData.push(data);
    console.log(undoData.length);
}

undo.onclick = function(){
    if(undoData.length < 1) return false;
    
    redoData.push(undoData[undoData.length - 1]);
    undoData.pop();
    ctx.putImageData(undoData[undoData.length - 1], 0, 0);
};

redo.onclick = function(){
  if(redoData.length < 1) return false;
  ctx.putImageData(redoData[redoData.length - 1], 0, 0);
  undoData.push(redoData.pop());
}

