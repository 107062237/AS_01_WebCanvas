# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
    
    畫面左側是顏色的選單，右側則是可以作為字體大小或畫筆粗細的滑桿，其大小由下至上為1px~30px。至於下方則是功能選單。
    要補充的一點是，我的icon下面是可以被畫圖的。

### Function description

    從左數過來第二個icon是可以有rainbow的效果，雖然說我沒有設邊界以至於他很有可能會爆掉。

### Gitlab page link

    your web page URL, which should be "https://107062237.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    趕作業好累==

<style>
table th{
    width: 100%;
}
</style>